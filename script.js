const books = [
	{
		author: "Люсі Фолі",
		name: "Список запрошених",
		price: 70
	},
	{
		author: "Сюзанна Кларк",
		name: "Джонатан Стрейндж і м-р Норрелл",
	},
	{
		name: "Дизайн. Книга для недизайнерів.",
		price: 70
	},
	{
		author: "Алан Мур",
		name: "Неономікон",
		price: 70
	},
	{
		author: "Террі Пратчетт",
		name: "Рухомі картинки",
		price: 40
	},
	{
		author: "Анґус Гайленд",
		name: "Коти в мистецтві",
	}
];
//create div with id="root"
const createDiv = document.createElement('div');
createDiv.setAttribute('id', 'root');
document.body.appendChild(createDiv);

const root = document.getElementById('root');
const ul = document.createElement('ul');

books.forEach(book => {
	if (!book.author || !book.name || !book.price) {
		console.error(`Invalid book: ${JSON.stringify(book)}`);
		return;
	}

	const li = document.createElement('li');
	li.textContent = `${book.author} , ${book.name} - ${book.price}`;
	li.style.listStyleImage = "url(./pic.png)";
	li.style.marginLeft = "50px";
	li.style.paddingBottom = "25px";
	ul.appendChild(li);
});

root.appendChild(ul);